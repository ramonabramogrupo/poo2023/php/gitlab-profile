![ramon](ramon.jpeg)

# Estructura completa del repositorio

- [ ] [Pagina Inicio](https://gitlab.com/ramonabramogrupo/poo2023)

- [X] Php
  - [ ] [Ejemplos](https://gitlab.com/ramonabramogrupo/poo2023/php/ejemplosphp/ejemplos/-/blob/master/readme.md?ref_type=heads)
  - [ ] Practicas

- [ ] Framework Php
- [ ] Yii
- [ ] Laravel

# Introducción al curso de PHP

PHP es un lenguaje de programación del lado del servidor que se utiliza para crear sitios web dinámicos e interactivos. Con PHP puedes trabajar con bases de datos, formularios, sesiones, cookies, archivos, y mucho más. PHP es uno de los lenguajes más populares y demandados en el mundo de la programación web.

En este curso aprenderás los fundamentos de PHP desde cero, empezando por los conceptos de programación estructurada y luego avanzando hacia la programación orientada a objetos. También aprenderás a usar PHP para interactuar con bases de datos MySQL y crear aplicaciones web completas.

## Objetivos del curso

Al finalizar este curso de PHP podrás:

- Comprender la sintaxis y la estructura de PHP.
- Utilizar variables, constantes, operadores, expresiones, y tipos de datos en PHP.
- Controlar el flujo de ejecución con sentencias condicionales y bucles.
- Definir y llamar funciones, y pasar argumentos por valor y por referencia.
- Trabajar con arreglos, cadenas, fechas, y archivos en PHP.
- Crear y manipular objetos con la programación orientada a objetos en PHP.
- Aplicar los principios de encapsulamiento, herencia, y polimorfismo en PHP.
- Conectar y consultar bases de datos MySQL con PHP.
- Validar y procesar datos de formularios con PHP.
- Manejar sesiones, cookies, y variables globales con PHP.
- Desarrollar proyectos web con PHP, como un sistema de registro y login, un blog, una galería de imágenes, y más.

![php](php.jpg)

## Contenido del curso

El curso se divide en las siguientes unidades:

- Unidad 1: Primeros pasos con PHP
  - Introducción al curso y al lenguaje PHP.
  - Instalación y configuración del entorno de desarrollo.
  - Sintaxis básica de PHP: etiquetas, comentarios, y salida.
  - Variables y constantes en PHP: declaración, asignación, y ámbito.
  - Operadores en PHP: aritméticos, de asignación, de comparación, lógicos, y de incremento y decremento.
  - Expresiones y tipos de datos en PHP: enteros, reales, booleanos, cadenas, y nulos.
  - Trabajoc con entradas y salidas en php
  - Trabajo con formularios
  - Formularios web: conceptos, elementos, y atributos.
  - Métodos de envío de datos: GET y POST.
  - Validación y procesamiento de datos de formularios con PHP.
  - Sesiones y cookies en PHP: conceptos, creación, y uso.
  - Variables globales en PHP: $_GET, $_POST, $_SESSION, $_COOKIE, y $_FILES.

- Unidad 2: Programación estructurada en PHP
  - Sentencias condicionales en PHP: if, else, elseif, y switch.
  - Bucles en PHP: while, do-while, for, y foreach.
  - Funciones en PHP: definición, llamada, parámetros, y retorno.
  - Paso de argumentos por valor y por referencia en PHP.
  - Funciones predefinidas en PHP: matemáticas, de cadenas, de fecha y hora, y de archivos.

- Unidad 3: Programación orientada a objetos en PHP
  - Introducción a la programación orientada a objetos: conceptos, ventajas, y aplicaciones.
  - Clases y objetos en PHP: definición, creación, y acceso.
  - Propiedades y métodos en PHP: declaración, asignación, y uso.
  - Modificadores de acceso en PHP: public, private, y protected.
  - Constructores y destructores en PHP: definición y uso.
  - Herencia en PHP: concepto, sintaxis, y ejemplos.
  - Polimorfismo en PHP: concepto, sobrecarga, y sobreescritura.
  - Clases y métodos abstractos en PHP: definición y uso.
  - Interfaces en PHP: definición, implementación, y uso.
  - Clases y métodos finales en PHP: definición y uso.
  - Clases y métodos estáticos en PHP: definición y uso.
  - Constantes de clase en PHP: definición y uso.

- Unidad 4: Bases de datos en PHP
  - Introducción a las bases de datos: conceptos, tipos, y modelos.
  - Introducción a MySQL: historia, características, y ventajas.
  - Instalación y configuración de MySQL.
  - Creación y administración de bases de datos y tablas con MySQL.
  - Lenguaje SQL: sentencias DDL, DML, y DCL.
  - Conexión y desconexión de bases de datos MySQL con PHP.
  - Consulta y manipulación de datos con PHP y MySQL.
  - Inserción, actualización, eliminación, y búsqueda de datos con PHP y MySQL.
  - Funciones y clases para trabajar con MySQL en PHP: mysqli y PDO.

- Unidad 5: Aplicaciones web con PHP
  - Introducción a las aplicaciones web: conceptos, características, y arquitecturas.  
  - Proyectos web con PHP: sistema de registro y login, blog, galería de imágenes, y más.
  - Realizar aplicaciones utilizando los conceptos de POO vistos
  - introduccion al paradigma de Modelo, Vista, Controlador

![logo](php_0.jpg)